void main() {
  var winningAppsOfMtn = [
    'Edtech',
    'Ambani Africa',
    'Khula',
    'Live Inspect',
    'Naked Insurance',
    'Shyft',
    'Hey Jude Women in Science, Technology, Engineering and Mathematics (STEM) Solution: Oru Social, TouchSA',
    'WunDrop',
    'EssyEquities',
    'FNB'
  ];

  winningAppsOfMtn.sort((a, b) => a.length.compareTo(b.length));
  print(winningAppsOfMtn);
  print("Winning app of 2017: ${winningAppsOfMtn[9]}");
  print("Winning app of 2018: ${winningAppsOfMtn[1]}");
  print(winningAppsOfMtn.length);
}
