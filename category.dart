class MtnWiningApp {
  String appName;
  String category;
  String developer;
  int year;

  MtnWiningApp(this.appName, this.category, this.developer, this.year) {}
}

void main(List<String> args) {
  MtnWiningApp nameOfApp = new MtnWiningApp(
      'Ambani Africa', 'Best South African App', 'Ambani', 2021);
  print(
      "The name of the app is: ${nameOfApp.appName} The category is: ${nameOfApp.category} The developer is: ${nameOfApp.developer} Year in: ${nameOfApp.year}");
}
